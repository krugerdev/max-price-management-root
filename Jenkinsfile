pipeline {

	agent any

	triggers {
	  pollSCM 'H/2 * * * *'
	}

	tools {
		gradle "Gradle 4.10.2"
	}

	options {
		buildDiscarder(logRotator(numToKeepStr:'3'))
        timeout(time: 30, unit: 'MINUTES')
    }

    stages {

        stage('Clean') {
            steps {
            	echo "Rama: ${env.BRANCH_NAME},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
				echo "Iniciando limpieza"
				sh 'gradle --console=plain clean'
            }
        }

        stage('Build') {
			steps {
				echo "Iniciando construccion"
				script {
					if ( env.BRANCH_NAME == 'master' ) {
						sh 'gradle --console=plain build --refresh-dependencies -x test -x check -x pmdMain -Penv=pro'
					}else {
						sh 'gradle --console=plain build --refresh-dependencies -x test -x check -x pmdMain -Penv=dev'
					}
				}
			}
		}

		stage('Test junit') {
			steps {
				echo "Iniciando pruebas unitarias"
				sh 'gradle --console=plain check -x pmdMain --stacktrace'
			}
		}

		stage('Test integration') {
			steps {
				echo "Iniciando pruebas de integracion"
				sh 'gradle --console=plain integrationTest --stacktrace'
			}
		}

		stage('Quality - Sonar - Docker') {
			parallel {

				stage('Quality') {
					steps {
						script {
							try {
								echo "Iniciando analisis de calidad del repositorio de la rama ${env.BRANCH_NAME}"
								sh 'gradle --console=plain sonarqube -x test -x check -x pmdMain'
							} catch(Exception e) {
								echo "Error en el analisis de calidad del repositorio de la rama ${env.BRANCH_NAME}"
							}
						}
					}
				}

				stage('Upload') {
					steps {
						echo "Iniciando Despliegue del repositorio de la rama ${env.BRANCH_NAME}"
						sh 'gradle --console=plain upload -x test -x check -x pmdMain'
					}
				}

				stage('Docker') {
					stages {

						stage('Build') {
							steps {
								echo "Iniciando la compilacion docker de la rama ${env.BRANCH_NAME}"
								sh 'gradle --console=plain docker -x test -x check -x pmdMain'
							}
						}

						stage('Upload') {
							steps {
								echo "Push docker de la rama ${env.BRANCH_NAME}"
								withDockerRegistry([ credentialsId: "dockerid", url: "https://docker.favorita.ec" ]) {
									sh 'gradle --console=plain dockerPush -x test'
				        		}
							}
						}
					}
				}
			}
		}
    }

	post {
		always {
			echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
		}
		success {
            echo 'La linea de construccion finalizo exitosamente'
			emailext(
				subject: "[Jenkins] La construccion fue exitosa. '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
				body: "La construccion fue exitosa.",
				recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
			)
		}

        failure {
            echo "La linea de construccion finalizo con errores ${currentBuild.fullDisplayName} with ${env.BUILD_URL}"
        }

        unstable {
            echo 'La linea de construccion finalizo de forma inestable'
        }
	}
}