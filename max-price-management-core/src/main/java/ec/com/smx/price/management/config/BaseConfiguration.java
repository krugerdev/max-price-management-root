package ec.com.smx.price.management.config;


import ec.com.kruger.spring.orm.jpa.config.SecurityAuditListenerConfig;
import ec.com.smx.frameworkv2.security.view.UserView;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.repository.PersonRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ec.com.kruger.spring.boot.config.JPAConfiguration;

/**
 * Base spring configuration.
 *
 * @author klopez on 2/20/19.
 * @version 1.0
 * @since 1.0.0
 */
@EnableJpaRepositories(basePackageClasses = PersonRepository.class)
@EntityScan(basePackageClasses = {PersonEntity.class, UserView.class})
@ComponentScan(basePackages = {"ec.com.smx.price.management"})
@EnableTransactionManagement
@Import({JPAConfiguration.class, SecurityAuditListenerConfig.class})
public class BaseConfiguration {

}
