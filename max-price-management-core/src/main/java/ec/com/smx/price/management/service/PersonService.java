package ec.com.smx.price.management.service;

import ec.com.kruger.spring.service.jpa.BaseService;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.exception.IdentifyNumberException;
import ec.com.smx.price.management.repository.IPersonRepository;
import ec.com.smx.price.management.vo.FindPersonRequest;
import ec.com.smx.price.management.vo.FindPersonResponse;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Person service implementation.
 *
 * @author klopez on 2/20/19.
 * @version 1.0
 * @since 1.0.0
 */
@Transactional("jpaTransactionManager")
@Validated
@Lazy
@Service
public class PersonService extends BaseService<PersonEntity, IPersonRepository> implements IPersonService {


    /**
     * Constructor with dependencies.
     *
     * @param repository The repository to inject
     */
    public PersonService(IPersonRepository repository) {
        super(repository);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public List<PersonEntity> findByFirstName(String firstName) {
        return repository.findByFirstName(firstName);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true, value = "jpaTransactionManager")
    @Override
    public List<FindPersonResponse> findPersons(FindPersonRequest personRequest) {
        return repository.findPersons(personRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveWithValidation(@NotNull final PersonEntity personEntity) {
        final PersonEntity entity = repository.findByIdentifyNumber(personEntity.getIdentifyNumber());
        if (entity != null) {
            throw new IdentifyNumberException("Entity with identify number already exist");
        }
        repository.save(personEntity);
    }
}
