package ec.com.smx.price.management.repository;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPQLQuery;
import ec.com.kruger.spring.orm.dto.SearchModelDTO;
import ec.com.kruger.spring.orm.jpa.repository.JPAQueryDslBaseRepository;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.vo.FindPersonRequest;
import ec.com.smx.price.management.vo.FindPersonResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

import static com.querydsl.core.types.Projections.bean;
import static ec.com.smx.price.management.entity.QPersonEntity.personEntity;

/**
 * Person repository compile with JPA and QUERYDSL.
 *
 * @author klopez on 2/20/19.
 * @version 1.0
 * @since 1.0.0
 */
@Lazy
@Repository
public class PersonRepository extends JPAQueryDslBaseRepository<PersonEntity> implements IPersonRepository {
    /**
     * Constructor.
     */
    public PersonRepository() {
        super(PersonEntity.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonEntity> findByFirstName(String firstName) {
        return from(personEntity).distinct().where(personEntity.status.isTrue(), personEntity.firstName.contains(firstName)).fetch();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FindPersonResponse> findPersons(FindPersonRequest personRequest) {
        BooleanExpression where = personEntity.status.isTrue();

        if (personRequest.getCodePerson() != null) {
            where = where.and(personEntity.codePerson.eq(personRequest.getCodePerson()));
        }

        if (StringUtils.isNotEmpty(personRequest.getFullName())) {
            where = where.and(personEntity.fullName.contains(personRequest.getFullName()));
        }

        if (StringUtils.isNotEmpty(personRequest.getFirstName())) {
            where = where.and(personEntity.firstName.contains(personRequest.getFirstName()));
        }

        if (StringUtils.isNotEmpty(personRequest.getLastName())) {
            where = where.and(personEntity.lastName.contains(personRequest.getLastName()));
        }

        return from(personEntity).select(bean(FindPersonResponse.class, personEntity.codePerson, personEntity.firstName,
                personEntity.lastName, personEntity.fullName)).where(where).fetch();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PersonEntity findByIdentifyNumber(final String identityNumber) {
        return from(personEntity).where(personEntity.status.isTrue(), personEntity.identifyNumber.eq(identityNumber)).fetchFirst();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<PersonEntity> findPagedData(Pageable pageable) {
        JPQLQuery<PersonEntity> query = from(personEntity).where(personEntity.statusString.eq("ACT"));
        return findPagedData(query, pageable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<PersonEntity> findPagedDataWithGenericFilters(Collection<SearchModelDTO<?>> searchModelDTOs, Pageable pageable) {
        JPQLQuery<PersonEntity> query = from(personEntity).where(personEntity.statusString.eq("ACT"));
        return findPagedData(query, pageable, searchModelDTOs);
    }
}
