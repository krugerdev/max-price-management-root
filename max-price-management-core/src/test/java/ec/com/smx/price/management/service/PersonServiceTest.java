package ec.com.smx.price.management.service;

import ec.com.smx.price.management.repository.PersonRepository;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.exception.IdentifyNumberException;
import ec.com.smx.price.management.vo.FindPersonRequest;
import ec.com.smx.price.management.vo.FindPersonResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for PersonService class.
 *
 * @author jlage on 01/03/2019.
 * @version 1.0
 * @since 1.0.0
 */

@RunWith(MockitoJUnitRunner.Silent.class)
public class PersonServiceTest {

    private static final String INVALID_IDENTIFY_NUMBER = "123456789";
    private static final String VALID_IDENTIFY_NUMBER = "1234";
    private static final String FIRST_NAME = "First name";

    private PersonEntity personEntity;

    @InjectMocks
    private PersonService personService;

    @Mock
    private PersonRepository personRepository;

    @Before
    public void setup() {
        personEntity = PersonEntity.builder().identifyNumber(INVALID_IDENTIFY_NUMBER).build();

        List<PersonEntity> personEntityList = new ArrayList<>();
        personEntityList.add(PersonEntity.builder().firstName(FIRST_NAME).build());

        when(personRepository.findByIdentifyNumber(INVALID_IDENTIFY_NUMBER)).thenReturn(personEntity);

        when(personRepository.findByFirstName(FIRST_NAME)).thenReturn(personEntityList);

        List<FindPersonResponse> findPersonResponseList = new ArrayList<>();
        findPersonResponseList.add(FindPersonResponse.builder().firstName(FIRST_NAME).build());

        when(personRepository.findPersons(FindPersonRequest.builder().firstName(FIRST_NAME).build()))
                .thenReturn(findPersonResponseList);
    }

    @Test(expected = IdentifyNumberException.class)
    public void testSaveWithValidationThrowException() {
        personService.saveWithValidation(personEntity);
    }

    @Test
    public void testSaveWithValidationVerifyNotSave() {
        try {
            personService.saveWithValidation(personEntity);
        } catch (IdentifyNumberException e) {
            verify(personRepository, times(1)).findByIdentifyNumber(INVALID_IDENTIFY_NUMBER);
            verify(personRepository, times(0)).save(personEntity);
        }
    }

    @Test
    public void testSaveWithValidationNotThrowException() {
        PersonEntity expectedPersonEntity = PersonEntity.builder().identifyNumber(VALID_IDENTIFY_NUMBER).build();
        personService.saveWithValidation(expectedPersonEntity);
        verify(personRepository, times(1)).findByIdentifyNumber(VALID_IDENTIFY_NUMBER);
        verify(personRepository, times(1)).save(expectedPersonEntity);
    }

    @Test
    public void testFindByFirstNameIsNotEmpty() {
        PersonEntity expectedPersonEntity = PersonEntity.builder().firstName(FIRST_NAME).build();
        List<PersonEntity> personEntityList = personService.findByFirstName(FIRST_NAME);
        PersonEntity personEntity = personEntityList.get(0);
        Assert.assertEquals("First name are not equals", expectedPersonEntity.getFirstName(), personEntity.getFirstName());
    }

    @Test
    public void testFindPersonsIsNotEmpty() {
        FindPersonRequest findPersonRequest = FindPersonRequest.builder().firstName(FIRST_NAME).build();
        FindPersonResponse expectedFindPersonResponse = FindPersonResponse.builder().firstName(FIRST_NAME).build();
        List<FindPersonResponse> findPersonResponseList = personService.findPersons(findPersonRequest);
        FindPersonResponse findPersonResponse = findPersonResponseList.get(0);
        Assert.assertEquals("First name are not equals", expectedFindPersonResponse.getFirstName(), findPersonResponse.getFirstName());
    }
}
