package ec.com.smx.price.management.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec.com.kruger.json.JsonPojoMapperBean;
import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.service.PersonService;
import ec.com.smx.price.management.vo.FindPersonRequest;
import ec.com.smx.price.management.vo.FindPersonResponse;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests para servicios rest y generacion de documentacion de
 * {@link PersonController} usando {@link MockMvc}.
 *
 * @author klopez
 * @version 1.0
 * @since 1.0.0
 */

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(PersonController.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersonControllerTest extends MockMvcControllerBase {

    @InjectMocks
    private PersonController personController;

    @Mock
    private PersonService personService;

    @Mock
    private ObjectMapper objectMapperMock;

    private ObjectMapper objectMapper = JsonPojoMapperBean.getInstance();

    private FindPersonRequest personRequest = FindPersonRequest.builder().lastName("Lopez").build();

    private PersonEntity personEntityResponse1 = PersonEntity.builder().codePerson(1).firstName("Kenny").lastName("Lopez").build();
    private FindPersonResponse personResponse1 = FindPersonResponse.builder().codePerson(1).firstName("Kenny").lastName("Lopez").build();

    private FindPersonResponse personResponse2 = FindPersonResponse.builder().codePerson(2).firstName("Maylin").lastName("Diaz").build();
    private FindPersonResponse personResponse3 = FindPersonResponse.builder().codePerson(2).firstName("Keylin").lastName("Lopez").build();
    private FindPersonResponse personResponse4 = FindPersonResponse.builder().codePerson(2).firstName("Kelly").lastName("Lopez").build();

    private List<FindPersonResponse> array1 = Arrays.asList(personResponse1, personResponse3, personResponse4);

    /**
     * Setup de los mock.
     * <p>
     * Nota: Se llama al setUpBase para inicializar el contexto solo con el
     * controlador que se desea probar.
     * </p>
     *
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Before
    public void setup() {
        super.setUpBase(personController);

        given(personService.findByFirstName("Kenny")).willReturn(Arrays.asList(personEntityResponse1));

        when(personService.findPersons(personRequest)).thenReturn(array1);
    }

    /**
     * Test find person
     *
     * @throws Exception
     */
    @Test
    public void testFindPerson() throws Exception {
        mockMvc
                .perform(post("/baseServices/api/person/findPersons").contextPath("/baseServices").content(objectMapper.writeValueAsString(personRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].lastName", is("Lopez")))
                .andExpect(jsonPath("$[1].lastName", is("Lopez")))
                .andExpect(jsonPath("$[2].lastName", is("Lopez")))
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * Test find person
     *
     * @throws Exception
     */
    @Test
    public void testFindPersonByFirstName() throws Exception {
        mockMvc
                .perform((get("/baseServices/api/person/findPersonsByFirstName").contextPath("/baseServices")).param("firstName", "Kenny")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstName", is("Kenny"))).andDo(MockMvcResultHandlers.print());
    }

}
