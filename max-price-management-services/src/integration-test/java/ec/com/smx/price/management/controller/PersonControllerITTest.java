package ec.com.smx.price.management.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import ec.com.kruger.spring.boot.config.JPAConfiguration;
import ec.com.kruger.spring.ws.common.WsConstants;
import ec.com.kruger.spring.ws.config.WebConfig;
import ec.com.kruger.spring.ws.controller.test.MockMvcContextBase;
import ec.com.smx.price.management.BaseSpringBootApplication;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.service.IPersonService;
import ec.com.smx.price.management.vo.FindPersonRequest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Tests para servicios rest y generacion de documentacion de
 * {@link PersonController} usando {@link MockMvc}.
 *
 * @author klopez
 * @version 1.0
 * @since 1.0.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = BaseSpringBootApplication.class)
@Import({WebConfig.class, JPAConfiguration.class})
@EnableTransactionManagement
public class PersonControllerITTest extends MockMvcContextBase {

    @Autowired
    private IPersonService personServices;

    private PersonEntity personResponse1 = PersonEntity.builder().firstName("Kenny").lastName("Lopez").build();
    private PersonEntity personResponse2 = PersonEntity.builder().firstName("Maylin").lastName("Diaz").build();
    private PersonEntity personResponse3 = PersonEntity.builder().firstName("Keylin").lastName("Lopez").build();
    private PersonEntity personResponse4 = PersonEntity.builder().firstName("Kelly").lastName("Lopez").build();


    /**
     * Setup de los mock.
     * <p>
     * Nota: Se llama al setUpBase para inicializar el contexto solo con el
     * controlador que se desea probar.
     * </p>
     *
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Before
    public void setup() throws JsonParseException, JsonMappingException, IOException {
        //personServices.save(personResponse1);
        //personServices.save(personResponse2);
        //personServices.save(personResponse3);
        //personServices.save(personResponse4);
    }

    /**
     * Test find person
     *
     * @throws Exception
     */
    @Test
    public void testFindPerson() throws Exception {

        FindPersonRequest personRequest = FindPersonRequest.builder().lastName("Lopez").build();

        mockMvc
                .perform(post("/baseServices/api/person/findPersons").contextPath("/baseServices").content(objectMapper.writeValueAsString(personRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string(WsConstants.REST_TYPE_HEADER, is(WsConstants.ResponseType.SUCCESS.toString())))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].lastName", is("Lopez")))
                .andExpect(jsonPath("$[1].lastName", is("Lopez")))
                .andExpect(jsonPath("$[2].lastName", is("Lopez")))
                .andDo(MockMvcResultHandlers.print());
    }


    /**
     * Test find person by first name
     *
     * @throws Exception
     */
    //@Test
    public void testFindPersonsByFirstName() throws Exception {

        mockMvc
                .perform(get("/baseServices/api/person/findPersonsByFirstName").contextPath("/baseServices").param("firstName", "Kenny")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(header().string(WsConstants.REST_TYPE_HEADER, is(WsConstants.ResponseType.SUCCESS.toString())))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstName", is("Maylin"))).andDo(MockMvcResultHandlers.print());
    }

    /**
     * Test find person by first name with empty required param
     *
     * @throws Exception
     */
    @Test
    public void testFindPersonsByFirstNameMissingParam() throws Exception {

        mockMvc
                .perform(get("/baseServices/api/person/findPersonsByFirstName").contextPath("/baseServices")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(header().string(WsConstants.REST_TYPE_HEADER, is(WsConstants.ResponseType.ERROR.toString())));
    }
}
