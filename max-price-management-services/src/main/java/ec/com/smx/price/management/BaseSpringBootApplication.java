package ec.com.smx.price.management;

import ec.com.kruger.security.sso.springboot2.configuration.SecurityKeycloakConfiguration;
import ec.com.kruger.spring.metric.config.DefaultApplicationContextMetricConfig;
import ec.com.kruger.spring.ws.config.WebConfig;
import ec.com.smx.frameworkv2.security.view.UserView;
import ec.com.smx.price.management.config.BaseConfiguration;
import ec.com.smx.price.management.entity.PersonEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

/**
 * Main class to run spring boot app.
 *
 * @author klopez on 2/21/19.
 * @version 1.0
 * @since 1.0.0
 */
@SpringBootApplication
@Import({BaseConfiguration.class, DefaultApplicationContextMetricConfig.class, WebConfig.class, SecurityKeycloakConfiguration.class})
@EntityScan(basePackageClasses = {PersonEntity.class, UserView.class})
@Slf4j
public class BaseSpringBootApplication extends SpringBootServletInitializer implements WebMvcConfigurer {

    /**
     * Main to run app.
     *
     * @param args args to pass app
     */
    public static void main(String... args) {
        SpringApplication.run(BaseSpringBootApplication.class, args);
    }

    /**
     * commandLineRunner.
     *
     * @param ctx the ctx
     * @return CommandLineRunner instance
     */
    @Bean
    public CommandLineRunner commandLineRunner(ListableBeanFactory ctx) {
        if (log.isDebugEnabled()) {
            log.debug("Beans Loaded by Spring Boot:{}", ctx.getBeanDefinitionCount());
        }
        return args -> {
            if (log.isDebugEnabled()) {
                String[] beanNames = ctx.getBeanDefinitionNames();
                Arrays.sort(beanNames);
                for (String beanName : beanNames) {
                    log.debug("Bean:{}", beanName);
                }
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST", "PUT", "DELETE");
    }


}
