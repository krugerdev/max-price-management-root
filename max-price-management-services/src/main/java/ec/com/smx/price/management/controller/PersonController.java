package ec.com.smx.price.management.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import ec.com.kruger.security.sso.springboot2.util.SecurityKeycloakUtil;
import ec.com.kruger.spring.ws.controller.BaseController;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.service.IPersonService;
import ec.com.smx.price.management.vo.FindPersonRequest;
import ec.com.smx.price.management.vo.FindPersonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class to controller person rest services.
 *
 * @author klopez on 2/21/19.
 * @version 1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/api/person")
@Lazy
@Slf4j
public class PersonController extends BaseController {

    @Lazy
    @Autowired
    private IPersonService personService;

    /**
     * Find person by some prefefields.
     *
     * @param person The person filter
     * @return Array of {@link FindPersonResponse}
     */
    @PostMapping("/findPersons")
    public List<FindPersonResponse> findPersons(@RequestBody FindPersonRequest person) {
        return personService.findPersons(person);
    }

    /**
     * Find persons by first name.
     *
     * @param firstName The first name to filter
     * @return Array of {@link PersonEntity}
     */
    @Secured("ROLE_SEARCH_PERSON")
    @PreAuthorize("hasRole('ROLE_SEARCH_PERSON')")
    //@PreAuthorize("hasAuthority('ROLE_SEARCH_PERSON')")
    @GetMapping("/findPersonsByFirstName")
    public List<PersonEntity> findPersonsByFirstName(@RequestParam String firstName) throws JsonProcessingException {
        log.info("CurrentUserLogin: {}", objectMapper.writeValueAsString(SecurityKeycloakUtil.getCurrentUserLogin()));
        return personService.findByFirstName(firstName);
    }

}
