package ec.com.smx.price.management.vo;

import ec.com.kruger.validation.javax.constraint.Cedula;
import ec.com.kruger.validation.javax.constraint.Ruc;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class VO for request find person process.
 *
 * @author klopez on 2019/2/20.
 * @version 1.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindPersonRequest {

    private Integer codePerson;

    private String firstName;

    private String lastName;

    private String fullName;

    @Cedula
    private String ci;

    @Ruc
    private String ruc;
}
