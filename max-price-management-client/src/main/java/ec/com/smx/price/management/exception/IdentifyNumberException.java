package ec.com.smx.price.management.exception;

/**
 * Manage exception for existing entity with identify number.
 *
 * @author jlage on 01/03/2019.
 * @version 1.0
 * @since 1.0.0
 */
public class IdentifyNumberException extends RuntimeException {

    /**
     * Personalize exception for identify number validations.
     *
     * @param message A message for exception
     */
    public IdentifyNumberException(String message) {
        super(message);
    }
}
