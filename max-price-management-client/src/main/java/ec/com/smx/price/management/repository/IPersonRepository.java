package ec.com.smx.price.management.repository;

import ec.com.kruger.spring.orm.dto.SearchModelDTO;
import ec.com.kruger.spring.orm.repository.IQueryDslBaseRepository;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.vo.FindPersonRequest;
import ec.com.smx.price.management.vo.FindPersonResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

/**
 * Person repository specification.
 *
 * @author klopez on 2019/2/20.
 * @version 1.0
 * @since 1.0.0
 */
public interface IPersonRepository extends IQueryDslBaseRepository<PersonEntity> {
    /**
     * Find persons by first name.
     *
     * @param firstName The first name
     * @return An array of person
     */
    List<PersonEntity> findByFirstName(String firstName);

    /**
     * Find persons by some filter.
     *
     * @param personRequest The person data
     * @return An array of person
     */
    List<FindPersonResponse> findPersons(FindPersonRequest personRequest);

    /**
     * Find first occurrence of entity with identifyNumber value.
     *
     * @param identityNumber The value for de identity number
     * @return An matched entity or null if it not exist
     */
    PersonEntity findByIdentifyNumber(final String identityNumber);

    /**
     * Find paged data.
     *
     * @param pageable The pagination information
     * @return The entities paginates
     */
    Page<PersonEntity> findPagedData(Pageable pageable);


    /**
     * Find paged data with generic filter.
     *
     * @param searchModelDTOs The generic filters
     * @param pageable        The pagination information
     * @return The entities paginates
     */
    Page<PersonEntity> findPagedDataWithGenericFilters(Collection<SearchModelDTO<?>> searchModelDTOs, Pageable pageable);
}
