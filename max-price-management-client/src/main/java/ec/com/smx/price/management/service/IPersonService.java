package ec.com.smx.price.management.service;

import ec.com.kruger.spring.service.IBaseService;
import ec.com.smx.price.management.entity.PersonEntity;
import ec.com.smx.price.management.vo.FindPersonRequest;
import ec.com.smx.price.management.vo.FindPersonResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Person services specification.
 *
 * @author klopez on 2019/2/20.
 * @version 1.0
 * @since 1.0.0
 */
public interface IPersonService extends IBaseService<PersonEntity> {
    /**
     * Find persons by first name.
     *
     * @param firstName The first name
     * @return An array of person
     */
    List<PersonEntity> findByFirstName(@NotEmpty String firstName);

    /**
     * Find persons by some filter.
     *
     * @param personRequest The person data
     * @return An array of person
     */
    List<FindPersonResponse> findPersons(@Valid FindPersonRequest personRequest);

    /**
     * Save person with an identity number validation.
     *
     * @param personEntity The entity to save
     */
    void saveWithValidation(@NotNull PersonEntity personEntity);
}
