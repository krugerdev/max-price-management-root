package ec.com.smx.price.management.entity;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Test id access person entity.
 *
 * @version 1.0
 * @autor klopez on 3/1/19.
 * @since 1.0.0
 */
public class PersonEntityTest {


    @Test
    public void testGetId() {
        PersonEntity person = PersonEntity.builder().codePerson(1).firstName("Kenny").lastName("Lopez")
                .fullName("Kenny Lopez").build();

        assertThat(person.getCodePerson(), is(person.getId()));
    }


}
